#include <iostream>
#include <string>

// I could also use struct here, then all members (functions and data)
// would be available per default
class lecture
{
public: // Available to anybody

    std::string date()
    {
        return "25 feb. 2015";
    }

protected: // Only available to derived classes

private: // Not available anywhere else

};

int main()
{
    // Allocated on the stack
    lecture lec1;
    std::cout << lec1.date() << std::endl;

    // In C++11 we should use smart pointers
    lecture* lec2 = new lecture();
    std::cout << lec2->date() << std::endl;
    delete lec2;

    return 0;
}
