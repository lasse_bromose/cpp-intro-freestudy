#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <cassert>
#include <memory>
#include <functional>

constexpr int sum(int a, int b)
{
  return a + b;
}

struct shape
{
  virtual double area() = 0;
};

struct square : public shape
{
  square(int side_length) : m_side_length(side_length){
    area(); /* Since square() uses this constructer, area will allways be initialized */
  }

  square() : square(0) /* Use the above constructer */
  { }

  double area()
  {
    return m_side_length * m_side_length;
  }

  int m_side_length;
};

int main()
{
  // Use at least 5 different modern C++11/14 features to update the following code

  // Question: fill the vector with values 1-10
  std::vector<int> v = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}; /* direct list initialization */
    
  // Question: Initialize mymap
  std::map<char,int> mymap = { {'b', 100}, {'a', 200}, {'c', 300} }; /* direct map initialization */

  // Question: show content of mymap:
  for(auto& kv: mymap) /*auto& -> object ref */
    std::cout << kv.first << " => " << kv.second << std::endl;
  
  // Question: Use smart_pointers to handle resource allocation
  std::shared_ptr<std::string> course(new std::string("C++11/14 free study activity"));

  if (course)
    {
      /* delete curse; Delete not needed as the ~shared_ptr frees the memory */ 
      course = nullptr;
    }

  // Question: Calculate value at compile time
  int number = sum(2,3); /* Changed sum to constexpr */

  // Question: Make sure a short is 2 bytes at compile time
  static_assert(sizeof(short) == 2, "Short is not of size 2");

  // Question: Make sure we override area and initialize m_side_length
  square s(3); /* See struct square */

  // Question: Call sum using a std::function
  std::function<int(int, int)> f_sum = sum;
  int mysum = f_sum(3,4);

  /* Lambda are awesome!!! */
  std::function<int(int, int)> lambda_sum = [](int a, int b){
    return a + b;
  }; 

  int my_lambdesum = lambda_sum(3, 4);

  return 0;
}
